module.exports = csvWilfrider;

function csvWilfrider() {
  var serviceName = 'CSV Wilfrider';
  var service = {
    getName: getName,
    showData: showData,
  };

  function getName() {
    return serviceName;
  }

  function showData(data) {
    console.log('"ID","DATA"\n1,"' + data + '"');
  }

  return service;
}
