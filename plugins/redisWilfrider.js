module.exports = redisWilfrider;

function redisWilfrider() {
  var serviceName = 'Redis Wilfrider';

  var service = {
    getName: getName,
    showData: showData,
  };

  function getName() {
    return serviceName;
  }

  function showData(data) {
    var obj = {
      id: 1,
      data: data,
    };
    console.log(JSON.stringify(obj));
  }

  return service;
}
