'use strict';

module.exports = copain;

function copain() {
  var config = require('./config/config.json');
  var wilfrider = require('./plugins/' + config.wilfrider)();
  var myScrappedData = 'C’est facile de comprendre les humains. ' +
    'Elles veulent seulement deux choses : ' +
    '1. Tout,' +
    '2. Et son contraire.';

  console.log('Using ' + wilfrider.getName() + '\n');
  console.log(wilfrider.showData(myScrappedData));

  return '';
};

copain();
